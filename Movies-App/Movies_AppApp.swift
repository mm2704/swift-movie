//
//  Movies_AppApp.swift
//  Movies-App
//
//  Created by Manuel Martin on 13/1/23.
//

import SwiftUI

@main
struct Movies_AppApp: App {
    var body: some Scene {
        WindowGroup {
            MovieListView()
        }
    }
}
