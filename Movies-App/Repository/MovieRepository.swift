//
//  MovieRepository.swift
//  Movies-App
//
//  Created by Manuel Martin on 19/1/23.
//

import SwiftUI
import Combine

class MovieRepository {
    
    private let apiKey = KeyGetter.shared.MOVIE_API_KEY
    private let urlDiscover = "https://api.themoviedb.org/3/discover/movie"
    private let urlDetail = "https://api.themoviedb.org/3/movie"
    private let movieNetwork = MovieNetwork.shared
    
    func getMovies(page: Int, sort: String?) -> AnyPublisher<Explorer, Error> {
        
        let queryItems = [URLQueryItem(name: "api_key", value: apiKey),
                          URLQueryItem(name: "language", value: "es-ES"),
                          URLQueryItem(name: "sort_by", value: sort),
                          URLQueryItem(name: "include_adult", value: "false"),
                          URLQueryItem(name: "include_video", value: "false"),
                          URLQueryItem(name: "page", value: String(page)),
                          URLQueryItem(name: "with_watch_monetization_types", value: "flatrate")
        ]
        return movieNetwork.getData(url: urlDiscover, queryItems: queryItems, typeClass: Explorer.self)
    }
    
    
    
    func getMovieDetail(movieId: Int) -> AnyPublisher<MovieDetail, Error> {
        
        let queryItems = [URLQueryItem(name: "api_key", value: apiKey),
                          URLQueryItem(name: "language", value: "es-ES")
        ]
        let url = urlDetail + "/\(movieId)"
        return movieNetwork.getData(url: url, queryItems: queryItems, typeClass: MovieDetail.self)
    }
}
