//
//  CellRowVideo.swift
//  Movies-App
//
//  Created by Manuel Martin on 19/1/23.
//

import SwiftUI

struct CellRowVideo : View {
    
    @State var showSheetDetail: Bool = false
    var movie: Movie
    
    var body: some View {
        
        Button(action: {
            showSheetDetail.toggle()
            
        }) {
            HStack {
                
                AsyncImage(url: URL(string: "https://image.tmdb.org/t/p/original\(movie.posterPath!)")) { image in
                    image.resizable()
                } placeholder: {
                    ProgressView()
                }
                .frame(width: UIScreen.main.bounds.width/2,
                       height:  UIScreen.main.bounds.height/2)
            }.sheet(isPresented: $showSheetDetail, content: {
                MovieListDetailView(movieImage: movie.posterPath!, movieId: movie.id)
            })
        }
    }
}
