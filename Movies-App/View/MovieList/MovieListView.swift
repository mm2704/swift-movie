//
//  ContentView.swift
//  Movies-App
//
//  Created by Manuel Martin on 13/1/23.
//

import SwiftUI

struct MovieListView: View {
    
    @StateObject var movieViewModel = MovieListViewModel()
    
    let layout = [GridItem(.flexible(), spacing: 0),
                  GridItem(.flexible(), spacing: 0)]
    
    var body: some View {
        ZStack {
                ScrollView {
                    LazyVGrid(columns: layout, spacing: 0) {
                        ForEach(movieViewModel.movies) { movie in
                            CellRowVideo(movie: movie)
                                .onAppear {
                                    movieViewModel.loadMoreContentIfNeeded(currentMovie: movie, sort: movieViewModel.currentSelected)
                                    
                                }
                        }
                    }
                }.edgesIgnoringSafeArea(.all)
            
            
            MenuView(currentSelected: movieViewModel.currentSelected, onChangeSelected: { newValue in
                movieViewModel.loadContent(pageSort: 1, sort: newValue)
            }, sortingName: Array<String>(movieViewModel.sortingDic.keys))
        }.onAppear {
            movieViewModel.loadContent(pageSort: nil, sort: movieViewModel.currentSelected)
            
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MovieListView()
    }
}
