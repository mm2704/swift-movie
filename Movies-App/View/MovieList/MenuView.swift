//
//  File.swift
//  Movies-App
//
//  Created by Manuel Martin on 19/1/23.
//

import SwiftUI


struct MenuView: View {
    
    @State var currentSelected: String
    var onChangeSelected: (String) -> Void
    let sortingName: [String]
    
    var body: some View {
        Menu {
            Picker("",selection: $currentSelected) {
                ForEach(sortingName, id:\.self) { value in
                    Text(value)
                        .tag(value)
                }
            }.onChange(of: self.currentSelected, perform: { _ in
                onChangeSelected(currentSelected)
            })
            
        } label: {
            Label("", systemImage: "line.3.horizontal.decrease.circle.fill")
        }.foregroundColor(.blue)
            .font(.system(size: 35))
            .frame(maxWidth: .infinity,
                   maxHeight: .infinity,
                   alignment: Alignment.bottomTrailing)
    }
}
