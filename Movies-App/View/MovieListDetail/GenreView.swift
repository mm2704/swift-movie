//
//  GenreView.swift
//  Movies-App
//
//  Created by Manuel Martin on 19/1/23.
//

import SwiftUI

struct GenreView: View {
    
    let genres: [Genre]
    
    let layout = [GridItem(.flexible(), spacing: 0),
                  GridItem(.flexible(), spacing: 0),
                  GridItem(.flexible(), spacing: 0),
                  GridItem(.flexible(), spacing: 0)]
    
    var body: some View {
        LazyVGrid(columns: layout, alignment: .center) {
            ForEach(self.genres) { genre in
                Text(genre.name)
                    .font(.system(size: 15, weight: .medium))
                    .foregroundColor(.white)
                    .lineLimit(1)
                    .padding(5)
                    .overlay(RoundedRectangle(cornerRadius: 15)
                        .stroke(.white))
                    .background(.black.opacity(0.3))
                    .cornerRadius(15)
            }
        }.padding()
    }
}
