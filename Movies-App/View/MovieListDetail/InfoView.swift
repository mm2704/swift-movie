//
//  InfoView.swift
//  Movies-App
//
//  Created by Manuel Martin on 19/1/23.
//

import SwiftUI

struct InfoView: View {
    
    let voteAverage: Float
    let runtime: Int
    let releaseDate: String
    
    var body: some View {
        HStack {
            let rate = String(format: "%.1f", self.voteAverage)
            Text("⭐️ \(rate)")
                .font(.system(size: 15, weight: .medium))
                .foregroundColor(.white)
                .frame(maxWidth: .infinity)
            Text("\(self.runtime) minutos")
                .font(.system(size: 15, weight: .medium))
                .foregroundColor(.white)
                .frame(maxWidth: .infinity)
            Text(self.releaseDate).font(.system(size: 15, weight: .medium))
                .foregroundColor(.white)
                .frame(maxWidth: .infinity)
            
        }.frame(minWidth: 0, maxWidth: .infinity)
            .background(.black.opacity(0.3))
    }
}
