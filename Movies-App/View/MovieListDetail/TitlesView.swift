//
//  TitlesView.swift
//  Movies-App
//
//  Created by Manuel Martin on 19/1/23.
//

import SwiftUI

struct TitlesView: View {
    
    let title: String
    let originalTitle: String
    
    var body: some View {
        VStack {
            Text(self.title)
                .font(.system(size: 32, weight: .medium))
                .multilineTextAlignment(.center)
                .foregroundColor(.white)
                .frame(minWidth: 0, maxWidth: .infinity)
                .background(.black.opacity(0.3))
                .padding(.top, 40)
            Text(self.originalTitle)
                .font(.system(size: 22, weight: .medium))
                .multilineTextAlignment(.center)
                .frame(minWidth: 0, maxWidth: .infinity)
                .background(.black.opacity(0.3))
                .foregroundColor(.white)
        }.padding(.bottom)
    }
}
