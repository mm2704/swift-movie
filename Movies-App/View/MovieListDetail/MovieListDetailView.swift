//
//  MovieListDetailView.swift
//  Movies-App
//
//  Created by Manuel Martin on 14/1/23.
//

import SwiftUI

struct MovieListDetailView: View {
    
    let movieImage: String
    let movieId: Int
    @StateObject var movieDetailViewModel = MovieListDetailViewModel()
    
    
    var body: some View {
        
        ZStack {
            AsyncImage(url: URL(string: "https://image.tmdb.org/t/p/original\(movieImage)")) { image in
                image.image?.resizable()
                    .edgesIgnoringSafeArea(.all)
                    .blur(radius: 5)
            }
            
            CloseButtonView()
            
            VStack {
                TitlesView(title: movieDetailViewModel.movieDetail?.title ?? "-",
                                          originalTitle: movieDetailViewModel.movieDetail?.originalTitle ?? "-")
                GenreView(genres: movieDetailViewModel.movieDetail?.genres ?? [])
                OverviewView(overview: movieDetailViewModel.movieDetail?.overview ?? "-")
                InfoView(voteAverage: movieDetailViewModel.movieDetail?.voteAverage ?? 0.0,
                         runtime: movieDetailViewModel.movieDetail?.runtime ?? 0,
                         releaseDate: movieDetailViewModel.movieDetail?.releaseDate ?? "-")
                Spacer()
            }
        }.onAppear{
            movieDetailViewModel.getMovieDetail(movieId: movieId)
        }
    }
}

struct MovieListDetailView_Previews: PreviewProvider {
    static var previews: some View {
        MovieListDetailView(movieImage: "/1NqwE6LP9IEdOZ57NCT51ftHtWT.jpg", movieId: 315162)
    }
}

