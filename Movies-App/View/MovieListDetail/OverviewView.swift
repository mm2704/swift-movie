//
//  OverviewView.swift
//  Movies-App
//
//  Created by Manuel Martin on 19/1/23.
//

import SwiftUI

struct OverviewView: View {
    
    let overview: String
    
    var body: some View {
        VStack {
            Text(self.overview)
                .font(.system(size: 18, weight: .medium))
                .foregroundColor(.white).padding(.bottom)
                .fixedSize(horizontal: false, vertical: true)
                .padding(.all)
        }.frame(minWidth: 0, maxWidth: .infinity)
            .background(.black.opacity(0.3))
    }
}
