//
//  CloseButtonView.swift
//  Movies-App
//
//  Created by Manuel Martin on 19/1/23.
//

import SwiftUI

struct CloseButtonView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        ZStack {
            Button(action: {
                presentationMode.wrappedValue.dismiss()
            }, label: {
                Image(systemName: "xmark")
                    .foregroundColor(.white)
                    .font(.system(size: 15))
                    .padding(15)
            })
        }.frame(
            maxWidth: .infinity,
            maxHeight: .infinity,
            alignment: Alignment.topLeading)
    }
}
