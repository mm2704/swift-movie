//
//  MovieListDetailViewModel.swift
//  Movies-App
//
//  Created by Manuel Martin on 14/1/23.
//

import SwiftUI
import Combine


class MovieListDetailViewModel: ObservableObject {
    
    @Published var movieDetail: MovieDetail?
    private var subscription: AnyCancellable?
    private let movieRepository = MovieRepository()
    
    
    public func getMovieDetail(movieId: Int) {
        subscription = movieRepository.getMovieDetail(movieId: movieId)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    print(error.localizedDescription)
                case .finished:
                    print("Success!")
                }
            }, receiveValue: { [weak self] value in
                guard let self = self else { return }
                self.movieDetail = MovieDetail(title: value.title,
                                          originalTitle: value.originalTitle,
                                          posterPath: value.posterPath,
                                          genres: value.genres,
                                          overview: value.overview,
                                          releaseDate: value.releaseDate,
                                          runtime: value.runtime,
                                          voteAverage: value.voteAverage)
                
            })
    }
    
}
