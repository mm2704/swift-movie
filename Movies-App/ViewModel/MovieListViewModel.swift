//
//  VideoListViewModel.swift
//  Movies-App
//
//  Created by Manuel Martin on 13/1/23.
//

import SwiftUI
import Combine

class MovieListViewModel: ObservableObject {
    
    @Published var movies: [Movie] = []
    var currentSelected: String = "Más Populares"
    var currentPage = 1
    var isLoadingPage = false
    var canLoadMorePages = true
    private let movieRepository = MovieRepository()
    
    let sortingDic = [ "Más Populares": "popularity.desc",
                       "Menos Populares": "popularity.asc",
                       "Más Votadas": "vote_average.desc",
                       "Menos Votadas": "vote_average.asc" ]
    
    private var subscription: AnyCancellable?
    
    
    
    func loadMoreContentIfNeeded(currentMovie movie: Movie?, sort: String) {
        guard let movie = movie else {
            loadMoreContent(sort: sort)
            return
        }
        
        let thresholdIndex = movies.index(movies.endIndex, offsetBy: -5)
        if movies.firstIndex(where: {$0.id == movie.id}) == thresholdIndex {
            loadMoreContent(sort: sort)
        }
    }
    
    private func loadMoreContent(sort: String) {
        guard !isLoadingPage && canLoadMorePages else {
            return
        }
        isLoadingPage = true
        loadContent(pageSort: nil, sort: sort)
    }
    
    
    public func loadContent(pageSort: Int?, sort: String) {
        
        let resultPageSort = pageSort ?? self.currentPage
        subscription = movieRepository.getMovies(page: resultPageSort, sort: sortingDic[sort]!)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    print(error.localizedDescription)
                    print(error)
                case .finished:
                    print("Success!")
                }
            }, receiveValue: { [weak self] value in
                guard let self = self else { return }
                
                let filteredMovies = self.filterMoviesWithNotPoster(movies: value.results)
                if(resultPageSort != 1){
                    self.movies = self.movies + filteredMovies
                } else {
                    self.movies = []
                    self.movies = filteredMovies
                }
                
                self.isLoadingPage = false
                self.currentPage = resultPageSort+1
                self.canLoadMorePages = value.page < value.totalPages
                
                if(filteredMovies.count < 10) {
                    self.loadMoreContent(sort: self.currentSelected)
                }
            })
    }
    
    private func filterMoviesWithNotPoster(movies: [Movie]) -> [Movie] {
        return movies.filter { movie in
            return movie.posterPath != nil
        }
    }
}
