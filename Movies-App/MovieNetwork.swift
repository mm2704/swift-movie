//
//  MovieNetwork.swift
//  Movies-App
//
//  Created by Manuel Martin on 13/1/23.
//

import SwiftUI
import Combine

class MovieNetwork {
    
    public static let shared: MovieNetwork = MovieNetwork()
    
    
    func getData<T: Codable>(url: String, queryItems: [URLQueryItem], typeClass: T.Type) -> AnyPublisher<T, Error> {
        
        var urlComps = URLComponents(string: url)!
        urlComps.queryItems = queryItems
        
        guard let url = urlComps.url else {
            let error = URLError(.badURL)
            return Fail(error: error).eraseToAnyPublisher()
        }
        
        return URLSession.shared.dataTaskPublisher(for: url)
            .receive(on: DispatchQueue.main)
            .tryMap ({ data, response -> Data in
                
                guard let httpResponse = response as? HTTPURLResponse else {
                    throw URLError(.unknown)
                }
                guard httpResponse.statusCode == 200 else {
                    let code = URLError.Code(rawValue: httpResponse.statusCode)
                    throw URLError(code)
                }
                return data
            })
            .decode(type: typeClass.self, decoder: JSONDecoder().addKeyDecodingStrategy())
            .eraseToAnyPublisher()
    }
}


extension JSONDecoder {
    func addKeyDecodingStrategy() -> JSONDecoder {
        self.keyDecodingStrategy = .convertFromSnakeCase
        return self
    }
}


