//
//  KeyGetter.swift
//  Movies-App
//
//  Created by Manuel Martin on 14/1/23.
//

import SwiftUI

class KeyGetter {
    
    public static let shared = KeyGetter()
    let dict: NSDictionary
    
    init() {
        guard let filePath = Bundle.main.path(forResource: "Keys", ofType: "plist"),
              let plist = NSDictionary(contentsOfFile: filePath) else {
            fatalError("Couldn´r find file 'Keys' plist")
        }
        self.dict = plist
    }
    
    var MOVIE_API_KEY: String {
        dict.object(forKey: "MOVIE_API_KEY") as? String ?? ""
    }
}

