//
//  MMMovie.swift
//  Movies-App
//
//  Created by Manuel Martin on 13/1/23.
//

import SwiftUI

struct Explorer : Codable {
    
    let page: Int
    let results: [Movie]
    let totalPages: Int
    let totalResults: Int
}
