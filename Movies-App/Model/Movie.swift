//
//  Movie.swift
//  Movies-App
//
//  Created by Manuel Martin on 13/1/23.
//

import SwiftUI

struct Movie : Identifiable, Codable {
    
    let id: Int
    let title: String
    let posterPath: String?
}
