//
//  MovieDetail.swift
//  Movies-App
//
//  Created by Manuel Martin on 14/1/23.
//

import SwiftUI

struct MovieDetail : Codable {
    
    let title: String
    let originalTitle: String
    let posterPath: String
    let genres: [Genre]
    let overview: String
    let releaseDate: String
    let runtime: Int
    let voteAverage: Float
    
}


struct Genre : Codable, Identifiable {
    let id: Int
    let name: String
}
